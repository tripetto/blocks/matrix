## <a href="https://tripetto.com/sdk/"><img src="https://unpkg.com/@tripetto/builder/assets/header.svg" alt="Tripetto FormBuilder SDK"></a>

🙋‍♂️ The *Tripetto FormBuilder SDK* helps building **powerful and deeply customizable forms for your application, web app, or website.**

👩‍💻 Create and run forms and surveys **without depending on external services.**

💸 Developing a custom form solution is tedious and expensive. Instead, use Tripetto and **save time and money!**

🌎 Trusted and used by organizations **around the globe**, including [Fortune 500 companies](https://en.wikipedia.org/wiki/Fortune_500).

---

*This SDK is the ultimate form solution for everything from basic contact forms to surveys, quizzes and more with intricate flow logic. Whether you're just adding conversational forms to your website or application, or also need visual form-building capabilities inside your app, Tripetto has got you covered! Pick what you need from the SDK with [visual form builder](https://tripetto.com/sdk/docs/builder/introduction/), [form runners](https://tripetto.com/sdk/docs/runner/introduction/), and countless [question types](https://tripetto.com/sdk/docs/blocks/introduction/) – all with [extensive docs](https://tripetto.com/sdk/docs/). Or take things up a notch by developing your [own question types](https://tripetto.com/sdk/docs/blocks/custom/introduction/) or even [form runner UIs](https://tripetto.com/sdk/docs/runner/custom/introduction/).*

---

## 📦 Matrix Block
[![Version](https://badgen.net/npm/v/@tripetto/block-matrix?icon=npm&label)](https://www.npmjs.com/package/@tripetto/block-matrix)
[![Downloads](https://badgen.net/npm/dt/@tripetto/block-matrix?icon=libraries&label)](https://www.npmjs.com/package/@tripetto/block-matrix)
[![License](https://badgen.net/npm/license/@tripetto/block-matrix?icon=libraries&label)](https://www.npmjs.com/package/@tripetto/block-matrix)
[![Read the docs](https://badgen.net/badge/icon/docs/cyan?icon=wiki&label)](https://tripetto.com/sdk/docs/blocks/stock/matrix/)
[![Source code](https://badgen.net/badge/icon/source/black?icon=gitlab&label)](https://gitlab.com/tripetto/blocks/matrix/)
[![Follow us on Twitter](https://badgen.net/badge/icon/@tripetto?icon=twitter&label)](https://twitter.com/tripetto)

This block for Tripetto implements a matrix question type. Use the matrix block to let respondents choose from a set of choices (columns) for multiple questions/statements (rows). The block includes a score feature that can be used to automatically calculate a score depending on the selected choices. Besides the [builder](https://tripetto.com/sdk/docs/builder/introduction/) part of the block, this package includes a base class with the validation/condition logic for the [runner](https://tripetto.com/sdk/docs/runner/introduction/) part of the block.

## 📺 Preview
[![Preview](https://unpkg.com/@tripetto/block-matrix/assets/preview.gif)](https://codepen.io/tripetto/live/BaJEpKR/4574a88c64dd7c38f0b94ad7496d7da4)

[![Try the demo](https://unpkg.com/@tripetto/builder/assets/button-demo.svg)](https://codepen.io/tripetto/live/BaJEpKR/4574a88c64dd7c38f0b94ad7496d7da4)

## 🚀 Get started
You can find all the information to start with this block at [tripetto.com/sdk/docs/blocks/stock/matrix/](https://tripetto.com/sdk/docs/blocks/stock/matrix/).

## 📖 Documentation
Tripetto has practical, extensive documentation. Find everything you need at [tripetto.com/sdk/docs/](https://tripetto.com/sdk/docs/).

## 🆘 Support
Run into issues or bugs? Report them [here](https://gitlab.com/tripetto/blocks/matrix/issues).

Need help or assistance? Please go to our [support page](https://tripetto.com/sdk/support/). We're more than happy to help you.

## 💳 License
Have a blast. [MIT](https://opensource.org/licenses/MIT).

## ✨ Contributors
- [Hisam A Fahri](https://gitlab.com/hisamafahri) (Indonesian translation)
- [Julian Frauenholz](https://gitlab.com/frauenholz) (German translation)
- [Krzysztof Kamiński](https://gitlab.com/kriskaminski) (Polish translation)

## 👋 About us
If you want to learn more about Tripetto or contribute in any way, visit us at [tripetto.com](https://tripetto.com/).
