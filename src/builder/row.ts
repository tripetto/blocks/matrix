/** Dependencies */
import {
    Collection,
    Forms,
    Markdown,
    Slots,
    alias,
    created,
    definition,
    deleted,
    editor,
    insertVariable,
    isBoolean,
    isString,
    markdownifyToString,
    name,
    pgettext,
    refreshed,
    renamed,
    reordered,
} from "@tripetto/builder";
import { Matrix } from "./";

export class Row extends Collection.Item<Matrix> {
    @definition("string")
    @name
    name = "";

    @definition("string", "optional")
    @alias
    alias?: string;

    @definition("string", "optional")
    explanation?: string;

    @created
    @reordered
    @renamed
    @refreshed
    defineSlot(): Slots.String {
        return this.ref.slots.dynamic({
            type: Slots.String,
            reference: this.id,
            label: pgettext("block:matrix", "Matrix row"),
            sequence: this.index,
            name:
                (this.name &&
                    markdownifyToString(
                        this.name,
                        Markdown.MarkdownFeatures.Formatting |
                            Markdown.MarkdownFeatures.Hyperlinks
                    )) ||
                undefined,
            alias: this.alias,
            exportable: this.ref.exportable,
            exchange: ["alias", "exportable"],
            pipeable: {
                label: pgettext("block:matrix", "Row"),
                legacy: "Row",
            },
        });
    }

    @deleted
    deleteSlot(): void {
        this.ref.slots.delete(this.id, "dynamic");
    }

    @editor
    defineEditor(): void {
        this.editor.option({
            name: pgettext("block:matrix", "Row"),
            form: {
                title: pgettext("block:matrix", "Row name"),
                controls: [
                    new Forms.Text(
                        "singleline",
                        Forms.Text.bind(this, "name", "")
                    )
                        .action("@", insertVariable(this))
                        .autoFocus()
                        .autoSelect()
                        .enter(this.editor.close)
                        .escape(this.editor.close),
                ],
            },
            locked: true,
        });

        this.editor.option({
            name: pgettext("block:matrix", "Description"),
            form: {
                title: pgettext("block:matrix", "Row description"),
                controls: [
                    new Forms.Text(
                        "multiline",
                        Forms.Text.bind(this, "explanation", undefined)
                    ).action("@", insertVariable(this)),
                ],
            },
            activated: isString(this.explanation),
        });

        this.editor.group(pgettext("block:matrix", "Options"));

        this.editor.option({
            name: pgettext("block:matrix", "Required"),
            form: {
                title: pgettext("block:matrix", "Required"),
                controls: [
                    new Forms.Checkbox(
                        pgettext(
                            "block:matrix",
                            "Row is required and cannot be skipped"
                        ),
                        Forms.Checkbox.bind(
                            this.defineSlot(),
                            "required",
                            undefined,
                            true
                        )
                    )
                        .locked(
                            (this.ref && isBoolean(this.ref.required)) || false
                        )
                        .on(() => this.refresh("name")),
                ],
            },
            activated: isBoolean(this.defineSlot().required),
            disabled: this.ref && isBoolean(this.ref.required),
        });

        this.editor.option({
            name: pgettext("block:matrix", "Identifier"),
            form: {
                title: pgettext("block:matrix", "Row identifier"),
                controls: [
                    new Forms.Text(
                        "singleline",
                        Forms.Text.bind(this, "alias", undefined)
                    ),
                    new Forms.Static(
                        pgettext(
                            "block:matrix",
                            "If a row identifier is set, this identifier will be used instead of the label."
                        )
                    ),
                ],
            },
            activated: isString(this.alias),
        });
    }
}
